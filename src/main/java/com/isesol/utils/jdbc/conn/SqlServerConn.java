package com.isesol.utils.jdbc.conn;


import java.sql.*;

public class SqlServerConn {


    public static void main(String args[]) {


        String connectionUrl = "jdbc:sqlserver://172.20.10.96:1433;"
                + "databaseName=Att2008;integratedSecurity=true;";

        String url = "jdbc:sqlserver://172.20.10.96:1433;databaseName=Att2008;user=checkin;password=Check,123";//sa身份连接

        String url2 = "jdbc:sqlserver://127.0.0.1:1368;databaseName=mydb;integratedSecurity=true;";//windows集成模式连接

        // Declare the JDBC objects.
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            // Establish the connection.
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url);

            // Create and execute an SQL statement that returns some data.
            String SQL = "SELECT * FROM userinfo";
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL);

            // Iterate through the data in the result set and display it.
            while (rs.next()) {
                System.out.println(rs.getString(1) + " " + rs.getString(4));
            }
        }

        // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null)
                try {
                    rs.close();
                } catch (Exception e) {
                }
            if (stmt != null)
                try {
                    stmt.close();
                } catch (Exception e) {
                }
            if (con != null)
                try {
                    con.close();
                } catch (Exception e) {
                }
        }
    }


}
