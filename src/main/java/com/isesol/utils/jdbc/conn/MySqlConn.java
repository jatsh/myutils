package com.isesol.utils.jdbc.conn;


import java.sql.*;

public class MySqlConn {


    public static void main(String[] args) {

        Connection con = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:mysql://localhost:3306/wj";
        String username = "root";
        String password = "123456";

        String sql = "select * from user_t ";


        try {
            con = DriverManager.getConnection(url, username, password);

        } catch (SQLException se) {

            System.out.println("数据库连接失败");
            se.printStackTrace();
        }

        try {
            String name = "";
            Statement statement = con.createStatement();


            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {

                String status = rs.getString("user_name");
                System.out.println(status);

                if (status.equals("404")) {
                    System.out.printf("not found ");
                    return;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
