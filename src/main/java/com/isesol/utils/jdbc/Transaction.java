package com.isesol.utils.jdbc;

import java.sql.*;

public class Transaction {

    private static Connection conn = null;
    private static String jdbcurl = "jdbc:mysql://localhost:3306/wj";
    private static String username = "root";
    private static String password = "123456";
    private static Savepoint savepoint1;

    public static void main(String[] args) throws Exception {


        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(jdbcurl, username, password);

        try {
            //Assume a valid connection object conn
            conn.setAutoCommit(false);
            Statement stmt = conn.createStatement();

            //set a Savepoint
            savepoint1 = conn.setSavepoint("Savepoint1");
            String SQL = "INSERT INTO user_t(user_name) " +
                    "VALUES ('test')";
            stmt.executeUpdate(SQL);

            //Submit a malformed SQL statement that breaks
//            SQL = "INSERTED IN user_t " +
//                    "VALUES (4,'name2','123',22)";
//            stmt.executeUpdate(SQL);
            // If there is no error, commit the changes.
            conn.commit();

        } catch (SQLException se) {
            // If there is any error.
            conn.rollback(savepoint1);
        }
    }
}
