package com.isesol.utils.jdbc;

import java.sql.*;

public class FetchSize {

    public static void main(String[] args) throws ClassNotFoundException {

        Connection con;

        String url = "jdbc:mysql://localhost:3306/wj";
        String username = "root";
        String password = "123456";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, username, password);
            con.setAutoCommit(false);
            com.mysql.jdbc.Statement statement = (com.mysql.jdbc.Statement) con.createStatement();

            //按行读取
            statement.setFetchSize(1);
            //打开流方式返回机制
            statement.enableStreamingResults();

            String sql = "select * from user_t ";
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                System.out.println(rs.getString("user_name"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
