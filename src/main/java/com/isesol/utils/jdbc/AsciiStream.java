package com.isesol.utils.jdbc;// Import required packages

import java.sql.*;
import java.io.*;

public class AsciiStream {


    static final String DB_URL = "jdbc:mysql://localhost:3306/wj";
    static final String USER = "root";
    static final String PASS = "123456";

    public static void main(String[] args) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        Statement stmt = null;
        ResultSet rs;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            createXMLTable(stmt);

            File f = new File("d:/XML_Data.xml");
            long fileLength = f.length();
            FileInputStream fis = new FileInputStream(f);

            String SQL = "INSERT INTO XML_Data VALUES (?,?)";

            pstmt = conn.prepareStatement(SQL);
            pstmt.setInt(1, 100);
            pstmt.setAsciiStream(2, fis, (int) fileLength);
            pstmt.execute();

            fis.close();

            SQL = "SELECT Data FROM XML_Data WHERE id=100";
            rs = stmt.executeQuery(SQL);

            if (rs.next()) {

                InputStream xmlInputStream = rs.getAsciiStream(1);
                int c;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                while ((c = xmlInputStream.read()) != -1)
                    bos.write(c);

                System.out.println(bos.toString());
            }

            rs.close();
            stmt.close();
            pstmt.close();
            conn.close();
        } catch (SQLException se) {

            se.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");
    }

    public static void createXMLTable(Statement stmt)
            throws SQLException {

        String streamingDataSql = "CREATE TABLE XML_Data " +
                "(id INTEGER, Data LONG)";
        try {
            stmt.executeUpdate("DROP TABLE XML_Data");
        } catch (SQLException se) {
        }

        stmt.executeUpdate(streamingDataSql);
    }
}