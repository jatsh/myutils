package com.isesol.utils.jdbc;

import java.sql.*;
import java.util.*;

/**
 * Created by june on 2017/1/18.
 */
public class Batch {

    private static Connection conn = null;
    private static String jdbcurl = "jdbc:mysql://localhost:3306/wj";
    private static String username = "root";
    private static String password = "123456";
    private static Savepoint savepoint1;


    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        String URL = "jdbc:mysql://localhost:3306/wj";
        Properties info = new Properties( );
        info.put( "user", "root" );
        info.put( "password", "123456" );

        conn = DriverManager.getConnection(URL, info);
        conn.setAutoCommit(false);

        Statement stmt = conn.createStatement();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j <100; j++) {
                String SQL = "INSERT INTO user_t (user_name) " +
                        "VALUES('test')";
                stmt.addBatch(SQL);
            }
            stmt.executeBatch();
            conn.commit();
        }
        System.out.println(System.currentTimeMillis()-start);

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            String SQL = "INSERT INTO user_t (user_name) " +
                    "VALUES('test')";
            stmt.execute(SQL);
            conn.commit();
        }
        System.out.println(System.currentTimeMillis()-start);

    }
}
