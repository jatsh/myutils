package com.isesol.utils.jdbc;

import java.sql.*;
import java.util.*;

/**
 * Created by june on 2017/1/19.
 */
public class CallableStatementDemo {


    public static void main(String[] args) throws SQLException {

        String URL = "jdbc:mysql://localhost:3306/wj";
        Properties info = new Properties();
        info.put("user", "root");
        info.put("password", "123456");
        Connection conn = DriverManager.getConnection(URL, info);

        CallableStatement cstmt = null;
        try {
            String SQL = "{call getEmpName (?, ?)}";
            cstmt = conn.prepareCall(SQL);
            cstmt.setInt(1, 1);

            cstmt.executeQuery();

            ResultSet rs = (ResultSet) cstmt.getObject(2);
            while (rs.next()) {
                System.out.println(rs.getString("FIRST"));
            }
        } catch (SQLException e) {

        } finally {
        }


    }
}
