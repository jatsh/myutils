package com.isesol.utils.jdbc.metInfo;

import com.google.common.collect.*;
import com.isesol.utils.jdbc.metInfo.entity.*;

import java.sql.*;
import java.util.*;

/**
 * Created by june on 2017/1/19.
 */
public class GetMetInfo {

    public List<TableInfoBean> getAllTable(String URL, String user, String password) throws SQLException {

        Properties info = new Properties();
        info.put("user", user);
        info.put("password", password);

        List<TableInfoBean> tableInfoBeanList = Lists.newArrayList();

        Connection conn = DriverManager.getConnection(URL, info);

        DatabaseMetaData databaseMetaData = conn.getMetaData();
        //获取所有表
        ResultSet tableSet = databaseMetaData.getTables(null, "%", "%", new String[]{"TABLE"});


        while (tableSet.next()) {

            String tableName = tableSet.getString("TABLE_NAME");

            String columnName;
            String columnType;

            ResultSet colRet = databaseMetaData.getColumns(null, "%", tableName, "%");
            List<ColumnBean> columnList = Lists.newArrayList();
            while (colRet.next()) {
                columnName = colRet.getString("COLUMN_NAME");
                columnType = colRet.getString("TYPE_NAME");
                int columnSize = colRet.getInt("COLUMN_SIZE");
                int digits = colRet.getInt("DECIMAL_DIGITS");
                int nullable = colRet.getInt("NULLABLE");

                ColumnBean columnBean = new ColumnBean();
                columnBean.setSqlType(columnType);
                columnBean.setColumnName(columnName);
                columnBean.setLength(columnSize);
                if (0 == nullable){
                    columnBean.setNullAble("not null");
                }
                columnList.add(columnBean);

            }

            TableInfoBean tableInfoBean = new TableInfoBean();
            tableInfoBean.setTableName(tableName);
            tableInfoBean.setColumnList(columnList);
            tableInfoBean.setTableComment(tableSet.getString("REMARKS"));

            tableInfoBeanList.add(tableInfoBean);
        }
        return tableInfoBeanList;
    }

}
