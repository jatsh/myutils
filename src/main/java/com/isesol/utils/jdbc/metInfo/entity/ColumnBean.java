package com.isesol.utils.jdbc.metInfo.entity;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ColumnBean {

    /**
     * 字段名称
     */
    private String columnName;

    /**
     * 字段长度
     */
    private int length;

    /**
     * 字段类型
     */
    private String SqlType;

    /**
     * comment
     */
    private String columnComment;

    /**
     * 是否可以null
     */
    public String nullAble;


}
