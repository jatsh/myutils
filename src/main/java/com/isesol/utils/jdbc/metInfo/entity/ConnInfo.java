package com.isesol.utils.jdbc.metInfo.entity;


import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConnInfo {

    private String url;

    private String name;

    private String password;
}
