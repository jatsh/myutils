package com.isesol.utils.jdbc.metInfo.entity;

import lombok.*;

import java.util.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TableInfoBean {

    private String tableName;

    private String tableComment;

    private List<ColumnBean> columnList;
}
