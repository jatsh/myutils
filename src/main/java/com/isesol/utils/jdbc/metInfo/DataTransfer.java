package com.isesol.utils.jdbc.metInfo;

import com.isesol.utils.jdbc.metInfo.entity.*;
import org.apache.commons.lang3.*;

import java.sql.*;
import java.util.*;

public class DataTransfer {

    public static void main(String[] args) throws SQLException {

        ConnInfo sourceInfo = ConnInfo.builder().name("root").password("123456").url("jdbc:mysql://localhost:3306/wj").build();
        ConnInfo destInfo = ConnInfo.builder().name("root").password("123456").url("jdbc:mysql://localhost:3306/wj2").build();

        copyDB(sourceInfo, destInfo);

    }

    public static void copyDB(ConnInfo sourceInfo, ConnInfo destInfo) throws SQLException {

        GetMetInfo metInfo = new GetMetInfo();
        List<TableInfoBean> tableInfoBeanList = metInfo.getAllTable(sourceInfo.getUrl(), sourceInfo.getName(), sourceInfo.getPassword());

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(destInfo.getUrl(), destInfo.getName(), destInfo.getPassword());
            Statement stmt = conn.createStatement();
            conn.setAutoCommit(false);

            for (TableInfoBean tableInfoBean : tableInfoBeanList) {

                List<ColumnBean> columnBeanList = tableInfoBean.getColumnList();
                String sql = "CREATE TABLE " + tableInfoBean.getTableName() + " (";

                for (ColumnBean columnBean : columnBeanList) {

                    if (StringUtils.isNotBlank(columnBean.getNullAble())) {
                        sql = sql + "" + columnBean.getColumnName() + " " + columnBean.getSqlType() + "(" + columnBean.getLength() + ") " + columnBean.getNullAble() + ",";
                    } else {
                        sql = sql + "" + columnBean.getColumnName() + " " + columnBean.getSqlType() + "(" + columnBean.getLength() + "),";
                    }
                }
                sql = sql + "PRIMARY KEY (" + columnBeanList.get(0).getColumnName() + "));";
                stmt.addBatch(sql);
            }
            stmt.executeBatch();
            conn.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            conn.rollback();
        }

    }
}
