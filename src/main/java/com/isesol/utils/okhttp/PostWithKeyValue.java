package com.isesol.utils.okhttp;

import com.squareup.okhttp.*;

import java.io.*;

public class PostWithKeyValue {

	static OkHttpClient client = new OkHttpClient();

	public static void main(String[] args) {

		RequestBody formBody = new FormEncodingBuilder()
				.add("platform", "android")
				.add("name", "bug")
				.add("subject", "XXXXXXXXXXXXXXX")
				.build();

		String url = "";
		try {
			System.out.println(post(url, formBody));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String post(String url, RequestBody formBody) throws IOException {

		Request request = new Request.Builder()
				.url(url)
				.post(formBody)
				.build();

		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return response.body().string();
		} else {
			throw new IOException("Unexpected code " + response);
		}
	}


}
