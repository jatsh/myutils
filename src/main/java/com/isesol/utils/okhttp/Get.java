package com.isesol.utils.okhttp;

import com.squareup.okhttp.*;

import java.io.*;

public class Get {

	static OkHttpClient client = new OkHttpClient();

	public static void main(String[] args) {

		String url = "http://10.1.60.34:8080/cgi?cmd=support/app/version/getAllCurrent";

		try {
			System.out.println(httpGet(url));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String httpGet(String url) throws IOException {

		Request request = new Request.Builder().url(url).build();
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return response.body().string();
		} else {
			throw new IOException("Unexpected code " + response);
		}
	}


}
