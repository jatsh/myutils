package com.isesol.utils.okhttp;

import com.alibaba.fastjson.*;
import com.isesol.utils.okhttp.model.*;
import com.squareup.okhttp.*;

import java.io.*;
import java.util.*;

public class PostWithJson {

	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	public static final OkHttpClient client = new OkHttpClient();

	public static void main(String[] args) {

		String url = "http://10.1.60.202:8080/cgi";
		String json = "{\"cmd\":\"support/app/version/getAllCurrent\"}";

		Map jsonMap = new HashMap();
		jsonMap.put("cmd", "support/app/version/update");
		Map paramsMap = new HashMap();
		paramsMap.put("appType", "android");
		paramsMap.put("channel", "");
		paramsMap.put("appCode", "isesol-rent");
		paramsMap.put("versionCode", "9");
		paramsMap.put("releaseDate", "2016-12-30 00:00:00");
		paramsMap.put("changelog", "");
		paramsMap.put("versionName", "v1.0.9");

		paramsMap.put("forced_update", true);

		jsonMap.put("parameters", paramsMap);
		 json = com.alibaba.fastjson.JSON.toJSONString(jsonMap);


		try {
			String response = post(url, json);
			JSONObject jsonObject = com.alibaba.fastjson.JSON.parseObject(response);
			if (null != jsonObject.get("error")){
				System.out.println(jsonObject.get("error").toString());
			}else {
				System.out.println("success!");
			}

//			System.out.println(response);
//			JSONObject jsonObject = com.alibaba.fastjson.JSON.parseObject(response);
//			JSONObject items = com.alibaba.fastjson.JSON.parseObject(jsonObject.get("response").toString());
//			List list = com.alibaba.fastjson.JSON.parseObject(items.get("items").toString(), new TypeReference<ArrayList>() {
//			});
//
//			for (int i = 0; i < list.size(); i++) {
//
//				SysAppVersion sysAppVersion = com.alibaba.fastjson.JSON.parseObject(list.get(i).toString(), SysAppVersion.class);
//				System.out.println(sysAppVersion.toString());
//			}


		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static String post(String url, String json) throws IOException {
		RequestBody body = RequestBody.create(JSON, json);
		Request request = new Request.Builder()
				.url(url)
				.post(body)
				.build();
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return response.body().string();
		} else {
			throw new IOException("Unexpected code " + response);
		}
	}


}
