package com.isesol.utils.okhttp.model;


import lombok.*;

import java.util.*;

/**
 * SysAppVersion
 *
 * @author peter zhang
 */
@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class SysAppVersion {

	/**
	 * 渠道
	 */
	private String channel;

	/**
	 * 版本号
	 */
	private String versionCode;

	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 发布日期
	 */
	private Date releaseDate;

	/**
	 * 是否强制更新
	 */
	private Boolean forcedUpdate;

	/**
	 * 更新地址
	 */
	private String updateUrl;

	/**
	 * 版本对应服务器地址
	 */
	private String serverUrl;

	/**
	 * App类型
	 */
	private String appType;

	/**
	 * App类型
	 */
	private String appCode;

	/**
	 * 是否是当前版本
	 */
	private Boolean isCurrent;

	/**
	 * 是否停止服务
	 */
	private Boolean isStopService;

	/**
	 * 升级说明
	 */
	private String changelog;

	/**
	 * 文件 md5
	 */
	private String md5;
}
