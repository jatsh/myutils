package com.isesol.utils.mybatis;

import org.apache.ibatis.io.*;
import org.apache.ibatis.session.*;

import java.io.*;

public class SessionFactory {


    public static void main(String[] args) throws IOException {

        String resource = "classpath:mybatis/mybatis-config.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

    }


}
