package com.isesol.utils.validtor;

import com.baidu.unbiz.fluentvalidator.*;
import com.isesol.utils.dto.*;
import org.apache.commons.lang3.*;

import java.util.regex.*;


public class RegisterValidator extends ValidatorHandler<RegisterDTO> implements Validator<RegisterDTO>{

    @Override
    public boolean validate(ValidatorContext context, RegisterDTO registerDTO) {

        boolean result = true;

        Pattern p = Pattern.compile("([a-zA-Z0-9_\\-.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})");
        Matcher matcher = p.matcher(registerDTO.getUserName());

        if (StringUtils.isBlank(registerDTO.getUserName())) {

            context.addErrorMsg("userName is null");
            result = false;
        }

        if (!matcher.matches()){

            context.addErrorMsg("有户名邮箱格式有误");
            result = false;
        }

        return result;

    }
}
