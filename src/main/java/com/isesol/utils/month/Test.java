package com.isesol.utils.month;


import java.text.*;
import java.util.*;

public class Test {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String[] args) throws Exception {

        check("2016-11-1", "2016-11-31");

    }


    public static void check(String startDate, String endDate) throws Exception {

        startDate = startDate + " 00:00:00";
        endDate = endDate + " 23:59:59";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date sDate = sdf.parse(startDate);
        Date eDate = sdf.parse(endDate);

        if (sDate.after(eDate)) {

            throw new Exception("开始日期在结束日期之前,start: " + startDate + ",endDate: " + endDate);
        }

        Calendar lastDate = Calendar.getInstance();
        lastDate.setTime(sdf.parse(startDate));
        lastDate.set(Calendar.DATE, 1);
        lastDate.add(Calendar.MONTH, 1);
        lastDate.add(Calendar.DATE, -1);

        if (eDate.after(lastDate.getTime())) {

            throw new Exception(endDate + " 越过本月最后一天," + lastDate.getTime());
        }


    }

}