package com.isesol.utils.redis;

import redis.clients.jedis.*;

import java.util.*;

public class RedisUtils {

	public static void main(String[] args) {

		System.out.println("=======connect test ===========");
		System.out.println();

		//连接本地的 Redis 服务
		Jedis jedis = new Jedis("localhost");
		System.out.println("Connection to server sucessfully");
		//查看服务是否运行
		System.out.println("Server is running: "+jedis.ping());


		/**
		 * string
		 */
		System.out.println("=======string test ===========");
		System.out.println();

		//设置 redis 字符串数据
		jedis.set("runoobkey", "Redis tutorial");
		// 获取存储的数据并输出
		System.out.println("Stored string in redis:: "+ jedis.get("runoobkey"));

		/**
		 * list
		 */
		System.out.println("=======list test ===========");
		System.out.println();

		//存储数据到列表中
		jedis.lpush("tutorial-list", "Redis");
		jedis.lpush("tutorial-list", "Mongodb");
		jedis.lpush("tutorial-list", "Mysql");
		// 获取存储的数据并输出
		List<String> list = jedis.lrange("tutorial-list", 0 ,100);
		for(int i=0; i<list.size(); i++) {
			System.out.println("Stored string in redis:: "+list.get(i));
		}



	}

}
