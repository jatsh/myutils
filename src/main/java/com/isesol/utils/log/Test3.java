package com.isesol.utils.log;

import org.slf4j.*;
import org.slf4j.spi.*;

/**
 * log4j over logback
 */
public class Test3 {

    public static Logger slf4jLogger = LoggerFactory.getLogger(Test3.class);

    public static void main(String[] args) {

        LocationAwareLogger locationAwareLogger = null;

        if (slf4jLogger instanceof LocationAwareLogger) {

            locationAwareLogger = (LocationAwareLogger)slf4jLogger;
        }

        locationAwareLogger.debug("log4j over logback");

    }
}
