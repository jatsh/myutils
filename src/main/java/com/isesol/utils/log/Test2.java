package com.isesol.utils.log;

import org.slf4j.*;

/**
 * slf4j + log4j
 */
public class Test2 {

    private static final Logger logger = LoggerFactory.getLogger(Test2.class);

    public void log() {

        logger.debug("Debug info.");
        logger.info("Info info");
        logger.warn("Warn info");
        logger.error("Error info");

    }

    public static void main(String[] args) {

        Test2 test = new Test2();
        test.log();
    }

}
