package com.isesol.utils.guava;

import com.google.common.collect.*;
import com.isesol.utils.entity.*;

import java.util.*;

public class Guava {


    Map<Person, List<BlogPost>> map = new HashMap<Person, List<BlogPost>>();

    public void addBlogPost(final Person author, final BlogPost blogPost) {
        List<BlogPost> blogPosts = map.get(author);
        if (blogPosts == null) {
            blogPosts = new ArrayList<BlogPost>();
            map.put(author, blogPosts);
        }
        blogPosts.add(blogPost);
    }

    Multimap<Person, BlogPost> multimap = ArrayListMultimap.create();

    public void addBlogPost2(final Person author, final BlogPost blogPost) {
        multimap.put(author, blogPost);
    }


    public static void main(String[] args) {

    }

}
