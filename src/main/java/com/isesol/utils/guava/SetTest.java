package com.isesol.utils.guava;

import com.google.common.collect.*;

import java.util.*;

import static com.google.common.collect.Sets.newHashSet;

/**
 * Created by admin on 2016/12/7.
 */
public class SetTest {

    public static void main(String[] args) {

        HashSet setA = newHashSet(1, 2, 3, 4, 5);
        HashSet setB = newHashSet(4, 5, 6, 7, 8);

        Sets.SetView union = Sets.union(setA, setB);
        System.out.println("union:");
        for (Object integer : union)
            System.out.println(integer);

        Sets.SetView difference = Sets.difference(setA, setB);
        System.out.println("difference:");
        for (Object integer : difference)
            System.out.println(integer);

        Sets.SetView intersection = Sets.intersection(setA, setB);
        System.out.println("intersection:");
        for (Object integer : intersection)
            System.out.println(integer);

        ImmutableMap mapA = ImmutableMap.of("a", "b", "c", "d");
        ImmutableMap mapB = ImmutableMap.of("a", "b", "c", "f");
        MapDifference differenceMap = Maps.difference(mapA, mapB);

    }
}
