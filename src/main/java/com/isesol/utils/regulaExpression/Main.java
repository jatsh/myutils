package com.isesol.utils.regulaExpression;

import java.util.regex.*;

public class Main {

    public static void main(String[] args) {

        Pattern p;
        Matcher matcher;

//        p = Pattern.compile("\\bwang\\b.*\\bjun\\b");
//        matcher = p.matcher("wang jun");
//        System.out.println(matcher.matches());
//
//        p = Pattern.compile("0\\d{2}-\\d{8}");
//        matcher = p.matcher("025-11111111");
//        System.out.println(matcher.matches());

//        p = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\\\\.[a-zA-Z0-9_-]{2,3}){1,2})$");
//        p = Pattern.compile("([a-zA-Z0-9_\\-.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})");
//        matcher = p.matcher("admin.me@wangjun.me");
//        System.out.println(matcher.matches());

        p = Pattern.compile("^/\\w+(.apk)$");
        matcher = p.matcher("/rent.apk");
        System.out.println(matcher.matches());


    }

}
